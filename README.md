# Root-Container

This repository stores a docker image with root and some other dependencies for systems that can run docker build don't have a root build installed.

To download the image: 

> docker pull registry.gitlab.com/pct-ma/root-container

Example run script: 

```bash
#!/bin/bash
# run-docker.sh 

PWD=`pwd`
USERID=`id -u`

docker run \
    -v $PWD/../:$PWD/../ \
    -w $PWD \
    -u $USERID \
    --rm -i registry.gitlab.com/pct-ma/root-container "$@"
```

Use it like 

> ./run-docker.sh cat /etc/system-release
