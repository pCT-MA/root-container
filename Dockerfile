FROM fedora

RUN yum -y update && yum clean all
RUN yum -y install erlang python-devel boost-devel git gcc boost-python3-devel \
    pugixml-devel gcc-c++ binutils make cmake3 eigen3-devel

RUN yum -y install root root-*

RUN yum -y install python3-pip

RUN pip3 install matplotlib uproot numpy pint scipy
